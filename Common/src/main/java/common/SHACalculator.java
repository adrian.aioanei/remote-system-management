package common;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.SequenceInputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SHACalculator {
	private SHACalculator() {}
	private static Logger logger = LoggerFactory.getLogger(SHACalculator.class);
	public static String calcMD5HashForDir(File dirToHash, boolean includeHiddenFiles) {

		if(!dirToHash.isDirectory())
			logger.error("File path {} is not a directory.", dirToHash);

		List<FileInputStream> fileStreams = new ArrayList<>();
		collectInputStreams(dirToHash, fileStreams, includeHiddenFiles);
		SequenceInputStream seqStream = new SequenceInputStream(Collections.enumeration(fileStreams));

		String md5Hash = null;
		try {
			md5Hash = DigestUtils.md5Hex(seqStream);
			seqStream.close();
		}
		catch (IOException e) {
			logger.error("Error reading files to hash in " + dirToHash.getAbsolutePath(), e);
		}
		return md5Hash;
	}

	private static void collectInputStreams(File dir,
			List<FileInputStream> foundStreams,
			boolean includeHiddenFiles) {

		File[] fileList = dir.listFiles();        
		for (File f : fileList) {
			if (!includeHiddenFiles && f.getName().startsWith(".")) {
				// Skip it
			}
			else if (f.isDirectory()) {
				collectInputStreams(f, foundStreams, includeHiddenFiles);
			}
			else {
				try {
					logger.info("\t {}", f.getAbsolutePath());
					foundStreams.add(new FileInputStream(f));
				}
				catch (FileNotFoundException e) {
					logger.error("File should never not be found: {}" , e.toString());
				}
			}
		}
	}
}
