package common;

import java.io.Serializable;

public class MessageFormat implements Serializable {
	private static final long serialVersionUID = 1L;
	private String host;
	private Long currentTime;
	private String SHAValue;
	
	public MessageFormat(String host,
				   Long currentTime,
				   String SHAValue) {
		
		this.host = host;
		this.currentTime = currentTime;
		this.SHAValue = SHAValue;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public Long getCurrentTime() {
		return currentTime;
	}

	public void setCurrentTime(Long currentTime) {
		this.currentTime = currentTime;
	}

	public String getSHAValue() {
		return SHAValue;
	}

	public void setSHAValue(String sHAValue) {
		SHAValue = sHAValue;
	}

	@Override
	public String toString() {
		return  this.host + " , " +
			    this.currentTime + " , "+ 
			    this.SHAValue;
	}
}
