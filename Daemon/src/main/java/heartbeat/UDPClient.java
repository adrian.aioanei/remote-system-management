package heartbeat;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

import org.apache.log4j.PropertyConfigurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import common.MessageFormat;
import common.SHACalculator;

public class UDPClient implements Serializable
{
	private static final long serialVersionUID = 1L;

	private static final Logger logger = LoggerFactory.getLogger(UDPClient.class);
	private AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext(Config.class);
	private Config config = ctx.getBean("clientConfigData", Config.class);

	public static void main( String[] args)
	{				
		logger.info("Start sending mesage...");		
		new UDPClient().setLog4jFormat().startReicevePackages();
		logger.info( "Finish sendig message..." );
	}

	private UDPClient setLog4jFormat() {
		PropertyConfigurator.configure(config.getLog4j());
		return this;
	}

	private void startReicevePackages() {
		DatagramSocket socket = null;
		DatagramPacket packet = null; 

		try {
			while(true) {
				InetAddress ip = InetAddress.getByName(config.getIp()); 
				int port = Integer.parseInt(config.getPort()); 
				byte[] buf = null; 

				MessageFormat messageForServer = new MessageFormat(InetAddress.getLocalHost().toString(),
													   System.currentTimeMillis(),
													   SHACalculator.calcMD5HashForDir(new File(config.getShaPathRootFolder()), true));	

				ByteArrayOutputStream bStream = new ByteArrayOutputStream();
				ObjectOutput oo = new ObjectOutputStream(bStream); 
				oo.writeObject(messageForServer);
				oo.close();
				buf = bStream.toByteArray(); 
				socket = new DatagramSocket();
				packet = new DatagramPacket(buf, buf.length, ip, port); 
				socket.send(packet);

				Thread.sleep(Integer.parseInt(config.getHeartBeatInterval()));
				logger.info("New message sent");
			}
		} catch (IOException e) {
			logger.error("IOException : {}", e.toString());
		} catch (NumberFormatException e) {
			logger.error("NumberFormatException : {}",e.toString());
		} catch (InterruptedException e) {
			logger.error("InterruptedException : {}", e.toString());
		} finally { 
			if(socket != null)
				socket.close();
		}
	}
}
