package heartbeat;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import java.io.File;

@Configuration
@ComponentScan("heartbeat")
@PropertySource(value = "clientConfig.properties", ignoreResourceNotFound=false)
public class Config {

	@Value("${server.ip}")
	public String ip;

	@Value("${server.port}")
	public String port;

	@Value("${path.to.logger.config.file}")
	public String log4j;
	
	@Value("${heartBeat.Interval}")
	public String heartBeatInterval;

	@Value("${root.path.for.shaCheck}")
	public String shaPathRootFolder;
	
	public String getShaPathRootFolder() {
		return shaPathRootFolder;
	}

	public void setShaPathRootFolder(String shaPathRootFolder) {
		this.shaPathRootFolder = shaPathRootFolder;
	}
	
	public String getHeartBeatInterval() {
		return heartBeatInterval;
	}

	public void setHeartBeatInterval(String heartBeatInterval) {
		this.heartBeatInterval = heartBeatInterval;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}

	public String getLog4j() {
		if (File.separatorChar == '/') {
			return log4j.replace('\\', File.separatorChar);
		} else {
			return log4j.replace('/', File.separatorChar);
		}
	}

	public void setLog4j(String log4j) {
		this.log4j = log4j;
	}

	@Bean("clientConfigData")
	public Config getConfigData() {
		Config dataSource = new Config();
		dataSource.setIp(ip);
		dataSource.setPort(port);
		dataSource.setLog4j(log4j);
		dataSource.setHeartBeatInterval(heartBeatInterval);
		dataSource.setShaPathRootFolder(shaPathRootFolder);
		return dataSource;
	}
}
