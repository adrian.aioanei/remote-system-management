package heartbeat;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.PropertyConfigurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import common.MessageFormat;

public class UDPServer implements Serializable
{
	private static final long serialVersionUID = 1L;
	private static Logger logger = LoggerFactory.getLogger(UDPServer.class);
	static ConcurrentHashMap<String, Long> clients = new ConcurrentHashMap<>();
	private static AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext(Config.class);
	private static Config config = ctx.getBean("serverConfigData", Config.class);
	
	private static Long updateInterval = Long.parseLong(config.getUpdateInterval());

	public static void main( String[] args ) throws InterruptedException
	{
		logger.info("Start receiving mesages...");

		Thread updateClientsThread = ThreadManagement.startUpdateClientsThread(updateInterval);
		Thread serverThread = ThreadManagement.startServerThread();

		updateClientsThread.start();
		serverThread.start();

		serverThread.join();
		updateClientsThread.join();
	}

	UDPServer setLog4jFormat() {
		PropertyConfigurator.configure(config.getLog4j());
		return this;
	}

	void startReicevePackages() {
		DatagramSocket socket = null; 
		byte[] buf = null; 
		int port = Integer.parseInt(config.getPort()); 

		try { 
			socket = new DatagramSocket(port); 
			while(true) {
				buf = new byte[1024]; 
				DatagramPacket packet = new DatagramPacket(buf, buf.length); 
				socket.receive(packet); 
				
				ObjectInputStream iStream = new ObjectInputStream(new ByteArrayInputStream(packet.getData()));
				MessageFormat messageClass = (MessageFormat) iStream.readObject();
				iStream.close();
								
				Utils.insertDataIntoMap(messageClass);
				Utils.printClientsDetails();
			}
		}catch(IOException e) {
			logger.error(e.toString());
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			if(socket != null)
				socket.close();
		}
	}

}
