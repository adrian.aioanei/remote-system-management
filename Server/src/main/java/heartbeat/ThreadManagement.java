package heartbeat;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ThreadManagement {
	private ThreadManagement() {}
	
	private static Logger logger = LoggerFactory.getLogger(ThreadManagement.class);

	static Thread startServerThread() {
		logger.info("Start thread " + Thread.currentThread().getId()) ;
		return new Thread() {
			@Override
			public void run() {
				new UDPServer().setLog4jFormat().startReicevePackages();
			}
		};
	}

	static Thread startUpdateClientsThread(Long updateInterval)  {
		return new Thread() {
			@Override
			public void run() {
				while(true) {
					Long currentTime = System.currentTimeMillis();
					for (Map.Entry<String, Long> entry : UDPServer.clients.entrySet()) {
						if (currentTime - entry.getValue() > updateInterval + 1500) {
							UDPServer.clients.remove(entry.getKey());
							logger.info("Remove client : {} (no heartbeat from this client in the last {} ms.)", entry.getKey(), updateInterval);
						}
					}
					try {
						Thread.sleep(updateInterval);
					} catch (InterruptedException e) {
						logger.info("startUpdateClientsThread : {}" ,e.toString());
					}
				}
			}
		};

	}

}
