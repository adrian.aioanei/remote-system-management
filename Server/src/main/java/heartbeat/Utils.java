package heartbeat;

import java.io.File;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import common.MessageFormat;
import common.SHACalculator;

public class Utils {
	private Utils() {}
	private static AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext(Config.class);
	private static Config config = ctx.getBean("serverConfigData", Config.class);
	private static Logger logger = LoggerFactory.getLogger(Utils.class);

	public static void insertDataIntoMap(MessageFormat msg) {
		if(msg != null) {
			UDPServer.clients.put(msg.getHost(), msg.getCurrentTime());	
			checkClientVersion(msg.getSHAValue());
			logger.info("Record added : host -> {}, time -> {}, sha -> {}", msg.getHost(),msg.getCurrentTime(), msg.getSHAValue());
		}
		else
			logger.error("The package received is not in a valid format.");
	}

	public static void printClientsDetails() {
		for (Map.Entry<String, Long> entry : UDPServer.clients.entrySet()) {
			logger.info("Client : {}, Time : {}",entry.getKey(), entry.getValue());
		}
	}
	
	public static void checkClientVersion(String sha) {
		if(!sha.equals(SHACalculator.calcMD5HashForDir(new File(config.getShaPathRootFolder()), true))) {
			logger.info("Client needs to update his jars.");
		}
	}
}
