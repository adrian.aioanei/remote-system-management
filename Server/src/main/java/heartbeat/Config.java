package heartbeat;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import java.io.File;

@Configuration
@ComponentScan("heartbeat")
@PropertySource(value = "serverConfig.properties", ignoreResourceNotFound=false)
public class Config {

	@Value("${server.port}")
	public String port;
	
	@Value("${path.to.logger.config.file}")
	public String log4j;

	@Value("${update.clients.interval}")
	public String updateInterval;
	
	@Value("${root.path.for.shaCheck}")
	public String shaPathRootFolder;
	
	public String getShaPathRootFolder() {
		return shaPathRootFolder;
	}

	public void setShaPathRootFolder(String shaPathRootFolder) {
		this.shaPathRootFolder = shaPathRootFolder;
	}

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}
	
	public String getLog4j() {
		if (File.separatorChar == '/') {
			return log4j.replace('\\', File.separatorChar);
		} else {
			return log4j.replace('/', File.separatorChar);
		}
	}

	public String getUpdateInterval() {
		return updateInterval;
	}

	public void setUpdateInterval(String updateInterval) {
		this.updateInterval = updateInterval;
	}

	public void setLog4j(String log4j) {
		this.log4j = log4j;
	}

	@Bean("serverConfigData")
	public Config getConfigData() {
		Config dataSource = new Config();
		dataSource.setPort(port);
		dataSource.setLog4j(log4j);
		dataSource.setUpdateInterval(updateInterval);
		dataSource.setShaPathRootFolder(shaPathRootFolder);
		return dataSource;
	}
}